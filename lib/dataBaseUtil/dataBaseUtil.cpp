#include "dataBaseUtil.h"

namespace dataBaseUtil
{

void setupDataBase()
{
    struct stat buffer;
    if (stat(DB_NAME, &buffer) != 0)
    {
        sqlite3 *DB;
        sqlite3_open(DB_NAME, &DB);

        const char table_data_battery[] = "CREATE TABLE DATA_BATTERY("
                                          "DATE_AQ DATE KEY NOT NULL,"
                                          "DEVICES TINYTEXT,"
                                          "STATES TINYTEXT NOT NULL,"
                                          "WARNING_LEVELS TINYTEXT NOT NULL,"
                                          "ENERGY TINYTEXT NOT NULL,"
                                          "ENERGY_EMPTY TINYTEXT NOT NULL,"
                                          "ENERGY_FULL TINYTEXT NOT NULL,"
                                          "ENERGY_FULL_DESIGN TINYTEXT NOT NULL,"
                                          "ENERGY_RATE TINYTEXT NOT NULL ,"
                                          "VOLTAGE TINYTEXT NOT NULL,"
                                          "PERCENTAGE TINYTEXT NOT NULL,"
                                          "CAPACITY TINYTEXT NOT NULL,"
                                          "TIME_TO_EMPTY TINYTEXT,"
                                          "TIME_TO_FULL TINYTEXT );";

        const char table_data_wifi[] = "CREATE TABLE DATA_WIFI("
                                       "DATE_AQ DATE KEY NOT NULL,"
                                       "DEVICES TINYTEXT,"
                                       "STATES TINYTEXT);";

        const char table_data_brightness[] = "CREATE TABLE DATA_BRIGHTNESS("
                                             "DATE_AQ DATE KEY NOT NULL,"
                                             "BRIGHTNESS FLOAT);";
        const char table_data_memory[] = "CREATE TABLE DATA_MEMORY("
                                         "DATE_AQ DATE KEY NOT NULL,"
                                         "MEMFREE INT,"
                                         "MEMAVAILABLE INT,"
                                         "MEMTOTAL INT,"
                                         "PERCENTAGE FLOAT);";

        const char table_data_thermal_device[] = "CREATE TABLE DATA_THERMAL_DEVICE("
                                                 "DATE_AQ DATE KEY NOT NULL,"
                                                 "THERMAL_DEVICE TINYTEXT,"
                                                 "CUR_STATE TINYTEXT,"
                                                 "MAX_STATE TINYTEXT,"
                                                 "TYPE TINYTEXT,"
                                                 "PERCENTAGE TINYTEXT);";

        const char table_data_thermal_zone[] = "CREATE TABLE DATA_THERMAL_ZONE("
                                               "DATE_AQ DATE KEY NOT NULL,"
                                               "TERMAL_ZONE TINYTEXT,"
                                               "TEMP TINYTEXT,"
                                               "TRIP_POINT_0_TEMP TINYTEXT,"
                                               "TRIP_POINT_0_TYPE TINYTEXT);";
        const char table_battery_manifacturer_info[] = "CREATE TABLE BATTERY_MANIFACTURER_INFO("
                                                       "SERIAL TINYTEXT KEY NOT NULL,"
                                                       "VENDOR TINYTEXT,"
                                                       "MODEL TINYTEXT,"
                                                       "NATIVE_PATH TINYTEXT);";
        sqlite3_stmt *stmt;
        const std::vector<std::string> statements = {table_battery_manifacturer_info, table_data_battery, table_data_wifi, table_data_brightness, table_data_memory,
                                                     table_data_thermal_device, table_data_thermal_zone};

        for (auto el : statements)
        {
            sqlite3_prepare_v2(DB, el.c_str(), -1, &stmt, NULL);
            // SQLITE_OK = 0 if ok a bit strange is this context
            if (SQLITE_OK)
            {
                throw std::invalid_argument("Error DB");
            }
            sqlite3_step(stmt);
            sqlite3_reset(stmt);
        }

        std::cout << "Database Created !" << std::endl;
        const std::string manifacturer_info = batteryInfoManifacturerStatement();
        sqlite3_prepare_v2(DB, manifacturer_info.c_str(), -1, &stmt, NULL);
        if (SQLITE_OK)
        {
            throw std::invalid_argument("Error DB");
        }
        sqlite3_step(stmt);
        sqlite3_reset(stmt);
        sqlite3_finalize(stmt);
        sqlite3_close(DB);
    }
}
std::string batteryInfoStatement(const std::string &p_dateAQ)
{

    std::string devices = "";
    std::string states = "";
    std::string warning_levels = "";
    std::string energy = "";
    std::string energy_empty = "";
    std::string energy_full = "";
    std::string energy_full_design = "";
    std::string energy_rate = "";
    std::string voltages = "";
    std::string percentage = "";
    std::string capacity = "";
    std::string time_to_empty = "";
    std::string time_to_full = "";

    std::string native_path = "";
    std::string vendor = "";
    const auto data = getAllBatteryInfo();
    for (auto el : data)
    {
        //battery info
        for (const auto &[key, value] : el.first)
        {
            if (key == "state")
            {
                states += withdrawUnitAddComma(value);
            }
            else if (key == "warning-level")
            {
                warning_levels += withdrawUnitAddComma(value);
            }
            else if (key == "energy")
            {
                energy += withdrawUnitAddComma(value);
            }
            else if (key == "energy-empty")
            {
                energy_empty += withdrawUnitAddComma(value);
            }
            else if (key == "energy-full")
            {
                energy_full += withdrawUnitAddComma(value);
            }
            else if (key == "energy-full-design")
            {
                energy_full_design += withdrawUnitAddComma(value);
            }
            else if (key == "energy-rate")
            {
                energy_rate += withdrawUnitAddComma(value);
            }
            else if (key == "voltage")
            {
                voltages += withdrawUnitAddComma(value);
            }
            else if (key == "percentage")
            {
                percentage += withdrawUnitAddComma(value);
            }
            else if (key == "capacity")
            {
                capacity += withdrawUnitAddComma(value);
            }
            else if (key == "time to empty")
            {
                time_to_empty += withdrawUnitAddComma(value);
            }
            else if (key == "time to full")
            {
                time_to_full += withdrawUnitAddComma(value);
            }
        }
        for (const auto &[key, value] : el.second)
        {
            if (key == "native-path")
            {
                devices += withdrawUnitAddComma(value);
                break;
            }
        }
    }
    const char sql_insert_colomn[] = "INSERT INTO DATA_BATTERY(DATE_AQ,DEVICES,STATES,WARNING_LEVELS,ENERGY,ENERGY_EMPTY,"
                                     "ENERGY_FULL,ENERGY_FULL_DESIGN,ENERGY_RATE,VOLTAGE,PERCENTAGE,CAPACITY,"
                                     "TIME_TO_EMPTY,TIME_TO_FULL)";
    const std::vector<std::string> values = {p_dateAQ, devices, states, warning_levels, energy, energy_empty,
                                             energy_full, energy_full_design, energy_rate, voltages,
                                             percentage, capacity, time_to_empty, time_to_full};

    const std::string sql_insert_value = "VALUES(" + concatenateInsertQueryText(values) + ");";
    std::stringstream ss;
    ss << sql_insert_colomn << std::endl
       << sql_insert_value;
    return ss.str();
}
std::string batteryInfoManifacturerStatement()
{
    std::string native_path = "";
    std::string vendor = "";
    std::string model = "";
    std::string serial = "";
    const auto data = getAllBatteryInfo();
    for (auto el : data)
    {
        //battery info
        for (const auto &[key, value] : el.second)
        {
            if (key == "native-path")
            {
                native_path += withdrawUnitAddComma(value);
            }
            else if (key == "vendor")
            {
                vendor += withdrawUnitAddComma(value);
            }
            else if (key == "model")
            {
                model += withdrawUnitAddComma(value);
            }
            else if (key == "serial")
            {
                serial += withdrawUnitAddComma(value);
            }
        }
    }
    const char sql_insert_colomn[] = "INSERT INTO BATTERY_MANIFACTURER_INFO(SERIAL,VENDOR,MODEL,NATIVE_PATH)";
    const std::vector<std::string> values = {serial, vendor, model, native_path};

    const std::string sql_insert_value = "VALUES(" + concatenateInsertQueryText(values) + ");";
    std::stringstream ss;
    ss << sql_insert_colomn << std::endl
       << sql_insert_value;
    return ss.str();
}

std::string wifiDataStatement(const std::string &p_dateAQ)
{
    const std::string DateAQ = p_dateAQ;

    const char sql_insert_colomn[] = "INSERT INTO DATA_WIFI(DATE_AQ,DEVICES,STATES)";
    const auto data = getAllWifiData();
    std::string states = "";
    std::string devices = "";
    for (auto el : data)
    {
        for (const auto &[key, value] : el)
        {
            devices += withdrawUnitAddComma(key);
            states += withdrawUnitAddComma(value ? "1" : "0");
        }
    }

    const std::vector<std::string> values = {p_dateAQ, devices, states};
    const std::string sql_insert_value = "VALUES(" + concatenateInsertQueryText(values) + ");";
    std::stringstream ss;
    ss << sql_insert_colomn << std::endl
       << sql_insert_value;
    return ss.str();
}
std::string screenBrightnessStatement(const std::string &p_dateAQ)
{
    const std::string DateAQ = p_dateAQ;
    const char sql_insert_colomn[] = "INSERT INTO DATA_WIFI(DATE_AQ,BRIGHTNESS)";
    const float data = getScreenBrightness();
    const std::string sql_insert_value = "VALUES('" + p_dateAQ + "'" + "'" + std::to_string(data) + "'" + ");";

    std::stringstream ss;
    ss << sql_insert_colomn << std::endl
       << sql_insert_value;
    return ss.str();
}
std::string memoryDataStatement(const std::string &p_dateAQ)
{
    const std::string DateAQ = p_dateAQ;
    const char sql_insert_colomn[] = "INSERT INTO DATA_MEMORY(DATE_AQ,MEMFREE,MEMAVAILABLE,MEMTOTAL,PERCENTAGE)";
    std::string mem_free = "";
    std::string mem_available = "";
    std::string mem_total = "";
    std::string percentage = "";
    const std::map<const std::string, double> data = getMemoryData();
    for (auto &[key, value] : data)
    {
        if (key == "MemFree")
        {
            mem_free += withdrawUnitAddComma(std::to_string(value));
        }
        else if (key == "MemAvailable")
        {
            mem_available += withdrawUnitAddComma(std::to_string(value));
        }
        else if (key == "MemTotal")
        {
            mem_total += withdrawUnitAddComma(std::to_string(value));
        }
        else if (key == "percentage")
        {
            percentage += withdrawUnitAddComma(std::to_string(value));
        }
    }
    const std::vector<std::string> values = {DateAQ, mem_free, mem_available, mem_total, percentage};
    const std::string sql_insert_value = "VALUES(" + concatenateInsertQueryText(values) + ");";
    std::stringstream ss;
    ss << sql_insert_colomn << std::endl
       << sql_insert_value;
    return ss.str();
}
std::pair<std::string, std::string> thermalDataStatement(const std::string &p_dateAQ)
{
    const char sql_insert_colomn_thermal_zone[] = "INSERT INTO DATA_THERMAL_ZONE(DATE_AQ,TERMAL_ZONE,TEMP,TRIP_POINT_0_TEMP,TRIP_POINT_0_TYPE)";
    const char sql_insert_colomn_devices[] = "INSERT INTO DATA_THERMAL_DEVICE(DATE_AQ,THERMAL_DEVICE,CUR_STATE,MAX_STATE,TYPE,PERCENTAGE)";
    std::vector<std::map<const std::string, std::string>> data = getAlltermalData();

    std::string cur_states = "";
    std::string max_states = "";
    std::string type_file_name = "";
    std::string temps = "";
    std::string trip_point_0_temps = "";
    std::string trip_point_0_type = "";
    std::string types = "";
    std::string devices = "";
    std::string thermal_zones = "";
    std::string percentages = "";
    for (auto el : data)
    {
        for (auto &[key, value] : el)
        {
            if (key == "cur_temp")
            {
                temps += withdrawUnitAddComma(value);
            }
            else if (key == "warning_temp")
            {
                trip_point_0_temps += withdrawUnitAddComma(value);
            }
            else if (key == "type_if_warning")
            {
                trip_point_0_type += withdrawUnitAddComma(value);
            }
            else if (key == "cur_state")
            {
                cur_states += withdrawUnitAddComma(value);
            }
            else if (key == "max_state")
            {
                max_states += withdrawUnitAddComma(value);
            }
            else if (key == "type")
            {
                types += withdrawUnitAddComma(value);
            }
            else if (key == "temp_zone")
            {
                thermal_zones += withdrawUnitAddComma(value);
            }
            else if (key == "cooling_device")
            {
                devices += withdrawUnitAddComma(value);
            }
            else if (key == "percentage")
            {
                percentages += withdrawUnitAddComma(value);
            }
        }
    }
    const std::vector<std::string> values_thermal_zones = {p_dateAQ, thermal_zones, temps, trip_point_0_temps, trip_point_0_type};
    const std::vector<std::string> values_thermal_device = {
        p_dateAQ,
        devices,
        cur_states,
        max_states,
        types, percentages};
    const std::string sql_insert_value_thermal_zones = "VALUES(" + concatenateInsertQueryText(values_thermal_zones) + ");";
    const std::string sql_insert_value_thermal_devices = "VALUES(" + concatenateInsertQueryText(values_thermal_device) + ");";

    std::stringstream ss_thermal_zones;
    std::stringstream ss_thermal_devices;
    ss_thermal_zones << sql_insert_colomn_thermal_zone << std::endl
                     << sql_insert_value_thermal_zones;
    ss_thermal_devices << sql_insert_colomn_devices << std::endl
                       << sql_insert_value_thermal_devices;
    return {ss_thermal_zones.str(), ss_thermal_devices.str()};
}
void insertIntoDataIntoBase(const std::string &p_dateAQ)
{
    sqlite3 *DB;
    sqlite3_open(DB_NAME, &DB);
    const std::pair<std::string, std::string> thermal_statements = thermalDataStatement(p_dateAQ);
    const std::vector<std::string> statementdb = {
        batteryInfoStatement(p_dateAQ),
        wifiDataStatement(p_dateAQ),
        screenBrightnessStatement(p_dateAQ),
        thermal_statements.first,
        thermal_statements.second};
    sqlite3_stmt *stmt;
    for (auto el : statementdb)
    {
        sqlite3_prepare_v2(DB, el.c_str(), -1, &stmt, NULL);
        // SQLITE_OK = 0 if ok a bit strange is this context
        if (SQLITE_OK)
        {
            throw std::invalid_argument("Error in data insertion");
        }
        sqlite3_step(stmt);
        sqlite3_reset(stmt);
    }
    sqlite3_finalize(stmt);
    sqlite3_close(DB);
}
std::string withdrawUnitAddComma(const std::string &el)
{
    return el.substr(0, el.find(" ")) + ",";
}
std::string concatenateInsertQueryText(const std::vector<std::string> &p_values)
{
    std::string query = "";
    for (auto el : p_values)
    {
        el = el.substr(0, el.rfind(","));
        query += "'" + el + "'" + ",";
    }
    return query.substr(0, query.length() - 1);
}
} // namespace dataBaseUtil
