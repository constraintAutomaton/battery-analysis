/**
 * \file dataBaseUtil.h
 * \author freeWanderer
 * function to send bulk data into the database
 */
#ifndef dataBaseUtil_h
#define dataBaseUtil_h
#include <sqlite3.h>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "daq.h"
#include <stdexcept>
#include <chrono>
#include <ctime>
#include <sys/stat.h>
#include <sstream>

namespace dataBaseUtil
{
const char DB_NAME[] = "batteryData.db";

void setupDataBase();
std::string batteryInfoStatement(const std::string &p_dateAQ);
std::string batteryInfoManifacturerStatement();

std::string wifiDataStatement(const std::string &p_dateAQ);
std::string screenBrightnessStatement(const std::string &p_dateAQ);
std::string memoryDataStatement(const std::string &p_dateAQ);
std::pair<std::string, std::string> thermalDataStatement(const std::string &p_dateAQ);

void insertIntoDataIntoBase(const std::string &p_dateAQ);


std::string withdrawUnitAddComma(const std::string &el);
std::string concatenateInsertQueryText(const std::vector<std::string> &p_values);
} // namespace dataBaseUtil
#endif
