/**
 * \file daq.h
 * \author freeWanderer
 * function to acquisition data relative to the battery consomption parameters
 */
#ifndef daq_h
#define daq_h
#include <string>
#include <map>
#include <vector>
#include <stdlib.h>
#include "pstream.h"
#include <string>
#include <sstream>
#include <map>
#include <iostream>
#include <fstream>
#include <utility>
#include <experimental/filesystem>
#include <chrono>
#include <ctime>
/**
 * \fn std::pair<std::map<const std::string, std::string>, std::map<const std::string, std::string>> reqBatteryInfo(std::string p_battery)
 * return the information about a query battery
 * \param p_battery battery queried .The information must be in a format that the command upower understand
 * \return {the information return are:
 * {
 *  "native-path",
 *  "vendor",
 *  "model",
 *  "serial"
 * },
 * 
 * { state
 *  warning-level
 *  energy
 *  energy-empty
 *  energy-full
 *  energy-full-design
 *  energy-rate
 *  voltage
 *  percentage
 *  capacity
 *  time to empty
 *  time to full
 * }
 * } 
 */
std::pair<std::map<const std::string, std::string>, std::map<const std::string, std::string>> getBatteryInfo(const std::string &p_battery);
/**
 * \fn std::vector<std::map<const std::string, std::string>> getAllBatteryInfo()
 * \param p_battery address of the battery
 * return the information of all the battery
 */
std::vector<std::pair<std::map<const std::string, std::string>, std::map<const std::string, std::string>>> getAllBatteryInfo();
/**
 * \fn std::vector<std::map<const std::string, bool>> getAllWifiData()
 * return the information about all the wifi devices
 */
std::vector<std::map<const std::string, bool>> getAllWifiData();
/**
 * \fn std::map<const std::string, bool> getWifiData(std::string p_wifi_device)
 * return the state of the wifi connection
 *  \return return the a string of the wifi device and true if connected
 */
std::map<const std::string, bool> getWifiData(const std::string &p_wifi_device);
/**
 * \fn float getScreenBrightness();\
 * \param p_wifi_device name of the wifi device
 * return the ration between the currrent and max brightness
 */
float getScreenBrightness();
/**
 * \fn std::map<const std::string, double> getMemoryData();
 * return the information about the memory usage
 * \return {
 *     return the memory free, the memory available, the total memory 
 *      and the percentage or memory used 
 * }
 */
std::map<const std::string, double> getMemoryData();
/**
 * \fn std::map<const std::string, double> getTermalData(std::string p_file, bool p_device = true);
 * \param p_folder folder of the component
 * \param p_cooling_device if true treat data as a cooling device
 * return the information about the temperature of the CPU and the cooling device
 * \return {
 * device:
 *  {
 *  cur_state,
 *  max_state,
 *  type
 * }
 * zone:
 * {
 * temp,
 * trip_point_0_temp,
 * trip_point_0_type
 * }
 * 
 * }
 */
std::map<const std::string, std::string> getTermalData(const std::string &p_folder, bool p_cooling_device = true);
/**
 * \fn std::vector<std::map<const std::string, double>> getAlltermalData();
 * return all the data related to temperature
 */
std::vector<std::map<const std::string, std::string>> getAlltermalData();
#endif