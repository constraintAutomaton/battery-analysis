#include "daq.h"
namespace fs = std::experimental::filesystem;

std::pair<std::map<const std::string, std::string>, std::map<const std::string, std::string>> getBatteryInfo(const std::string &p_battery)
{
    const std::vector<std::string> keys =
        {
            "state",
            "warning-level",
            "energy",
            "energy-empty",
            "energy-full",
            "energy-full-design",
            "energy-rate",
            "voltage",
            "percentage",
            "capacity",
            "time to empty",
            "time to full"};
    const std::vector<std::string> manifacturier_keys =
        {
            "native-path",
            "vendor",
            "model",
            "serial"};
    std::map<const std::string, std::string> info_battery;
    std::map<const std::string, std::string> battery_manifacturer_info;
    std::stringstream ss;

    redi::ipstream proc("upower -i " + p_battery, redi::pstreams::pstdout | redi::pstreams::pstderr);
    std::string line;

    const int indexValue = 25;
    const int indexManifacturerIndex = 24;
    while (std::getline(proc.out(), line))
    {
        for (std::string key : keys)
        {
            if (line.find(key) != std::string::npos)
            {
                info_battery.insert(std::make_pair(key, line.substr(indexValue)));
            }
        }
        // could be simpler in a single for loop
        for (std::string key : manifacturier_keys)
        {
            if (line.find(key) != std::string::npos)
            {
                battery_manifacturer_info.insert(std::make_pair(key, line.substr(indexManifacturerIndex)));
            }
        }
    }
    // read child's stderr
    while (std::getline(proc.err(), line))
    {
        ss << "stderr: " << line << '\n';
    }
    if (ss.str().empty() != true)
    {
        std::cout << ss.str() << std::endl;
    }
    proc.close();
    return {info_battery, battery_manifacturer_info};
}
std::vector<std::pair<std::map<const std::string, std::string>, std::map<const std::string, std::string>>> getAllBatteryInfo()
{
    std::stringstream ss;
    std::string line;
    const std::string battery_key_word = "battery";
    std::vector<std::pair<std::map<const std::string, std::string>, std::map<const std::string, std::string>>> v_battery_info;

    redi::ipstream proc("upower -e", redi::pstreams::pstdout | redi::pstreams::pstderr);

    while (std::getline(proc.out(), line))
    {

        if (line.find(battery_key_word) != std::string::npos)
        {
            v_battery_info.push_back(getBatteryInfo(line));
        }
    }

    while (std::getline(proc.err(), line))
    {
        ss << "stderr: " << line << '\n';
    }
    std::cout << ss.str() << std::endl;
    return v_battery_info;
}
std::vector<std::map<const std::string, bool>> getAllWifiData()
{
    std::stringstream ss;
    std::string line;
    redi::ipstream proc("nmcli connection show", redi::pstreams::pstdout | redi::pstreams::pstderr);
    const int index_device = 72;
    std::vector<std::map<const std::string, bool>> infos;

    while (std::getline(proc.out(), line))
    {
        if (line.substr(index_device).find("DEVICE") == std::string::npos &&
            line.substr(index_device).find("--") == std::string::npos)
        {
            std::string device = line.substr(index_device);
            device = device.substr(0, device.find(" "));
            infos.push_back(getWifiData(device));
        }
    }

    while (std::getline(proc.err(), line))
    {
        ss << "stderr: " << line << '\n';
    }
    std::cout << ss.str() << std::endl;
    return infos;
}
std::map<const std::string, bool> getWifiData(const std::string &p_wifi_device)
{
    std::stringstream ss;
    std::string line;
    std::map<const std::string, bool> info;
    const std::string property = "GENERAL.STATE:";
    const std::string connected = "100";

    redi::ipstream proc("nmcli -f GENERAL,WIFI-PROPERTIES dev show " + p_wifi_device, redi::pstreams::pstdout | redi::pstreams::pstderr);

    while (std::getline(proc.out(), line))
    {
        if (line.find(property) != std::string::npos && line.find(connected) != std::string::npos)
        {
            info.insert(std::make_pair(p_wifi_device, true));
            break;
        }
    }
    while (std::getline(proc.err(), line))
    {
        ss << "stderr: " << line << '\n';
    }
    if (ss.str().empty() != true)
    {
        std::cout << ss.str() << std::endl;
    }
    proc.close();
    return info;
}
float getScreenBrightness()
{
    std::string line;
    std::ifstream actual_brightness_file("/sys/class/backlight/intel_backlight/actual_brightness");
    float actual_brightness = 0;
    float max_brightness = 1;
    while (std::getline(actual_brightness_file, line))
    {
        actual_brightness = std::stoi(line);
    }
    std::ifstream max_brightness_file("/sys/class/backlight/intel_backlight/max_brightness");
    while (std::getline(max_brightness_file, line))
    {
        max_brightness = std::stoi(line);
    }
    actual_brightness_file.close();
    max_brightness_file.close();
    return actual_brightness / max_brightness;
}
std::map<const std::string, double> getMemoryData()
{
    std::string line;
    std::vector<std::string> info_tag = {"MemFree", "MemAvailable", "MemTotal"};

    std::ifstream memory_data_file("/proc/meminfo");
    std::map<const std::string, double> memory_data;
    const int index_last_number = 24;
    while (std::getline(memory_data_file, line))
    {
        for (std::string el : info_tag)
        {
            if (line.find(el) != std::string::npos)
            {

                const int index_last_space = line.substr(0, index_last_number).rfind(" ");
                const long value = std::stol(line.substr(index_last_space, index_last_number - index_last_space));
                memory_data.insert(std::make_pair(el, value));
            }
        }
    }
    const double percentage = (memory_data["MemFree"] / memory_data["MemTotal"]) * 100;
    memory_data.insert(std::make_pair("percentage", percentage));
    memory_data_file.close();
    return memory_data;
}
std::map<const std::string, std::string> getTermalData(const std::string &p_folder, bool p_cooling_device)
{
    std::string line;
    std::map<const std::string, std::string> data;
    if (p_cooling_device == true)
    {
        const std::string cur_state_file_name = "cur_state";
        const std::string max_state_file_name = "max_state";
        const std::string type_file_name = "type";

        std::ifstream cur_state_file(p_folder + "/" + cur_state_file_name);
        std::ifstream max_state_file(p_folder + "/" + max_state_file_name);
        std::ifstream type_file(p_folder + "/" + type_file_name);

        std::getline(cur_state_file, line);
        data.insert(std::make_pair("cur_state", line));

        std::getline(max_state_file, line);
        data.insert(std::make_pair("max_state", line));

        std::getline(type_file, line);
        data.insert(std::make_pair("type", line));

        const double cur_state = std::stod(data["cur_state"]);
        const double max_state = std::stod(data["max_state"]);

        data.insert(std::make_pair("percentage", std::to_string(cur_state / max_state)));
        data.insert(std::make_pair("cooling_device", p_folder));
    }
    else
    {
        const std::string cur_temp_file_name = "temp";
        const std::string warning_temp_file_name = "trip_point_0_temp";
        const std::string type_if_warning_file_name = "trip_point_0_type";

        std::ifstream cur_temp_file(p_folder + "/" + cur_temp_file_name);
        std::ifstream warning_temp_file(p_folder + "/" + warning_temp_file_name);
        std::ifstream type_if_warning_file(p_folder + "/" + type_if_warning_file_name);

        std::getline(cur_temp_file, line);
        data.insert(std::make_pair("cur_temp", line));

        std::getline(warning_temp_file, line);
        data.insert(std::make_pair("warning_temp", line));

        std::getline(type_if_warning_file, line);
        data.insert(std::make_pair("type_if_warning", line));

        data.insert(std::make_pair("temp_zone", p_folder));
    }

    return data;
}
std::vector<std::map<const std::string, std::string>> getAlltermalData()
{
    std::vector<std::map<const std::string, std::string>> data;
    fs::path root_thermal_data("/sys/class/thermal");

    for (auto &dir : fs::directory_iterator(root_thermal_data))
    {
        std::stringstream ss;
        ss << dir;
        std::string dir_string = ss.str();
        dir_string.replace(dir_string.find("\""), 1, "");
        dir_string.replace(dir_string.find("\""), 1, "");

        if (dir_string.find("cooling_device") == std::string::npos)
        {
            data.push_back(getTermalData(dir_string, false));
        }
        else
        {
            data.push_back(getTermalData(dir_string, true));
        }
    }
    return data;
}
