#include <iostream>
#include "dataBaseUtil.h"
#include <chrono>
#include <ctime>
using namespace std;

int main()
{
     auto now = std::chrono::system_clock::now();
     time_t time_now = std::chrono::system_clock::to_time_t(now);
     std::stringstream ss;
     ss << std::ctime(&time_now);
     const std::string dateAQ = ss.str().substr(0, ss.str().length() - 1);

     dataBaseUtil::setupDataBase();
     dataBaseUtil::insertIntoDataIntoBase(dateAQ);
     return 0;
}